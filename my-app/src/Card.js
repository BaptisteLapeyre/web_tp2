import React from 'react'
import './Card.css';

export default class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {card: [], deck: [], valide: false}
        
        fetch('https://virtserver.swaggerhub.com/caro3801/Ligueofstones/1.0.0/cards')
        .then(response => response.json())
        .then((response)=>{
            this.setState({card: response});
        });
    }

    handleClickCard = event => {
        if (this.state.deck.length < 20){
            const {id} = event.target;
            let listdeck = []; 
            listdeck = listdeck.concat(this.state.deck);
            let listcard = []; 
            listcard = listcard.concat(this.state.card); //c'est pas beau mais si je recupere juste this.state.deck la reference ne change pas et le vue ne se met donc pas a jour
            for(let i=0; i<this.state.card.length;i++){
                if (this.state.card[i].id==id){
                    listdeck.push(this.state.card[i]);
                    listcard.splice(i,1);
                    break;
                }
            }
            this.setState({
                card: listcard,
                deck: listdeck
            });
        }
        
    }

    handleClickDeck = event => {
        if (!this.state.valide) {
            const {id} = event.target;
            let listdeck = []; 
            listdeck = listdeck.concat(this.state.deck);
            let listcard = []; 
            listcard = listcard.concat(this.state.card); //c'est pas beau mais si je recupere juste this.state.deck la reference ne change pas et le vue ne se met donc pas a jour
            for(let i=0; i<this.state.deck.length;i++){
                if (this.state.deck[i].id==id){
                    listcard.push(this.state.deck[i]);
                    listdeck.splice(i,1);
                    break;
                }
            }
            this.setState({
                card: listcard,
                deck: listdeck
            });
        }
        
    }

    handleClickValide = event => {
        this.setState({valide: true});
    }

    render() {
        const mescarte = this.state.card.map(element => {
            const im = "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/"+element.key+"_0.jpg"
            return (
                <div className="card col-3 bg-dark p-0 m-2 zoom" id={element.id} onClick={this.handleClickCard}>
                    <img className="card-img-top " src={im} id={element.id} alt=""></img>
                    <p className="card-text text-light bg-dark p-0 m-0" id={element.id}> Attaque : {element.info.attack}</p>
                    <p className="card-text text-light bg-dark p-0 m-0" id={element.id}> Armure : {element.info.defense}</p>
                    <p className="card-footer text-light p-0 m-0 bg-danger" id={element.id}>{element.key} </p>
                </div>
            );
        });
        const mondeck = this.state.deck.map(element => {
            const im = "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/"+element.key+"_0.jpg"
            return (
                <div className="card col-3 bg-dark p-0 m-2 zoom" id={element.id} onClick={this.handleClickDeck}>
                    <img className="card-img-top " src={im} id={element.id} alt=""></img>
                    <p className="card-text text-light bg-dark p-0 m-0" id={element.id}> Attaque : {element.info.attack}</p>
                    <p className="card-text text-light bg-dark p-0 m-0" id={element.id}> Armure : {element.info.defense}</p>
                    <p className="card-footer text-light p-0 m-0 bg-danger" id={element.id}>{element.key} </p>
                </div>
            );
        });
        let valide;
        if (this.state.deck.length >= 20) {
            valide = <button  className=" col-5 bg-success" onClick={this.handleClickValide}>j'ai fini</button>
        }
         
        if (this.state.valide){
            return <div className="col-12 bg-success" >
            <h1 className=" col-12 justify-content-center">le deck a été validé! </h1>
            <div className="row col-12 pl-2 justify-content-center ">{mondeck}</div>
          </div>
        }


        return <div className="container-fluid row p-0 m-0 ">
            <div className=" col-6"  >
        <div className="row">
            <h1 className=" col-12 justify-content-center text-light">Champions disponibles</h1>
        </div>
            <div className="row col-12 pl-2 justify-content-center p-0 m-0">{mescarte}</div>
      </div>

      <div className="col-6 " >
        <h1 className=" col-12 justify-content-center text-light">Deck  </h1> 
        {valide}
        <div className="row col-12 pl-2 justify-content-center p-0 m-0">{mondeck}</div>
      </div></div>
        

    }
}